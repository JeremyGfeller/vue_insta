import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import product1 from './views/product1.vue'
import product2 from './views/product2.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/product1',
      name: 'product1',
      component: product1
    },
    {
      path: '/product2',
      name: 'product2',
      component: product2
    }
  ]
})
